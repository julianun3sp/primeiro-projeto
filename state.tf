terraform {
  backend "s3" {
    bucket = "base-config-rm344957"
    key    = "gitlab-iac"
    region = "us-east-1"
  }
}
